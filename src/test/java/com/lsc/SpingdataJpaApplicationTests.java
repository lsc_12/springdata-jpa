package com.lsc;


import com.lsc.pojo.ToandMany.Student;
import com.lsc.pojo.ToandMany.Teacher;
import com.lsc.pojo.User;
import com.lsc.repository.CRUDRepository;
import com.lsc.repository.PageRepository;
import com.lsc.repository.ToandMany.StudentRepository;
import com.lsc.repository.ToandMany.TeacherRepository;
import com.lsc.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;


import java.util.List;
import java.util.Optional;


@SpringBootTest
class SpingdataJpaApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CRUDRepository crudRepository;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

//    测试按名字查询
    @Test
    public void test() {

        List<User> userList = this.userRepository.USER_LIST("张三","12346");

        System.out.println(userList);

    }
//测试更新
    @Test
    public void UpdateUser(){
        userRepository.Update_User("张三",4);

    }

//    测试删除用户
    @Test
    public void DeleteUser(){
        userRepository.DeleteUser(4);
    }

//    测试模糊查询
    @Test
    public void Usernamelike() {

        List<User> users = this.userRepository.findByUsernameLike("%李%");
        for (User user : users) {
            System.out.println(user);
        }
    }

//    测试查找全部
    @Test
    public void CrudQueryUser() {

        List<User> all = (List<User>) this.crudRepository.findAll();
        System.out.println(all);
    }

//    测试通过id查找全部数据
    @Test
    public void CrudQueryUserById() {

        Optional<User> user = this.crudRepository.findById(3);
        System.out.println(user);
    }

//    测试插入数据
    @Test

    public void CrudSelectUSer() {

        User user = new User();
        user.setUsername("清华");
        user.setPassword("123456");
        user.setAddress("广东省河源市");
        this.crudRepository.save(user);
    }

//测试排序  DESC是倒序  ASC是正序
    @Test
    public void SortUser() {
        /*排序*/
        List<User> all = (List<User>) this.pageRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));

        for (User user : all) {
            System.out.println(user);
        }

    }
//测试分页
    @Test
    public void PageUser() {
        // 分页
        Page<User> userPage = this.userRepository.findAll(PageRequest.of(0, 2, Sort.by(Sort.Direction.DESC, "id")));

        for (User user : userPage) {
            System.out.println(user);
        }
        //        计算分页总条数
        System.out.println("数据总条数"+userPage.getTotalElements());
        //        计算总页数
        System.out.println("数据总页数"+userPage.getTotalPages());

    }


    @Test
    public void StudentManyAndOne(){

        Student student = new Student();
        student.setStudentName("小李");

        Teacher teacher = new Teacher();
        teacher.setTeacherName("张老师");

        teacher.getStudents().add(student);

        studentRepository.save(student);
        teacherRepository.save(teacher);
    }

}

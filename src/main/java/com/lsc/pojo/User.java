package com.lsc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @program: spring_Data_jpa
 * @description: 用户实体类
 * @author: 上河图
 * @create: 2020-10-02 16:41
 */

/*  @Entity:声明这个类是一个实体类
    @Table：实体类映射表
        name:数据库中表的名称
*
* */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "User_jpa")
public class User {

    /*
         @Id:声明这是一个主键
         @GeneratedValue：主键生成策略 像自增策略
            strategy: 策略生成方式
                GenerationType.IDENTITY:自增  用于mysql
                GenerationType.SEQUENCE:自增  用于oracle
                GenerationType.AUTO:自增 根据系统来选策略生成模式
          @Column: 对应数据库表中的字段
                name：字段的名称
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private String username;


    private String password;


    private String address;


}

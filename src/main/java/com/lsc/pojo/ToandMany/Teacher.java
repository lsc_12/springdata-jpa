package com.lsc.pojo.ToandMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @program: spingdata_jpa
 * @description: 教师测试类
 * @author: 上河图
 * @create: 2020-10-05 16:40
 */


/*
*
  @Data 是lombok插件的
  @Entity 是说明这个类是一个实体类
  @Table 实体类和数据库映射
  @Column 加的是数据库的字段
* */
@Data
@Entity
@Table(name = "teacher")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "teacherid")
    private Integer teacherid;

    @Column(name = "teacherName")
    private String teacherName;

    @Column(name = "teacherAge")
    private Integer teacherAge;

    /*
    *  @JoinColumn
    *       name: 外键名字
    *       referencedColumnName：外键对应的键
    * */

    @OneToMany(targetEntity = Student.class)
    @JoinColumn(name = "teacher_id",referencedColumnName = "teacherid")
    private Set<Student> students =  new HashSet<>();




}

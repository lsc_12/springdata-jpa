package com.lsc.pojo.ToandMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

/**
 * @program: spingdata_jpa
 * @description: 学生测试类
 * @author: 上河图
 * @create: 2020-10-05 16:37
 */

/*
  @Data 是lombok插件的
  @Entity 是说明这个类是一个实体类
  @Table 实体类和数据库映射
* */
@Data
@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "studentid")
    private Integer studentid;

    @Column(name = "StudentName")
    private String StudentName;

    @Column(name = "StudentSchool")
    private String StudentSchool;

    /*
     *  @JoinColumn
     *       name: 外键名字
     *       referencedColumnName：外键对应的键
     * */
    @ManyToOne(targetEntity = Teacher.class)
    @JoinColumn(name ="teacher_id",referencedColumnName = "teacherid")
    private Teacher teacher;


}

package com.lsc.repository;

import com.lsc.pojo.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: spingdata_jpa
 * @description: 测试页数和排序
 * @author: 上河图
 * @create: 2020-10-05 09:33
 */

@Repository
public interface PageRepository extends PagingAndSortingRepository<User,Integer> {

}

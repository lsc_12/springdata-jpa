package com.lsc.repository;

import com.lsc.pojo.User;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * @program: spingdata_jpa
 * @description: 操作数据的持久层，相当于Dao层
 * @author: 上河图
 * @create: 2020-10-03 16:18
 */

@Repository
public interface UserRepository extends PagingAndSortingRepository<User,Integer> {
    //方法名称必须要遵循驼峰式命名规则，findBy（关键字）+属性名称（首字母大写）+查询条件（首字母大写）
    //    自定义方法
//    如果有@Query注解，方法名都不用遵循命名规则
    @Query("select u from User u where u.username=?1 and password =?2")
    List<User> USER_LIST(String Username,String Password);

//    模糊查询
    List<User> findByUsernameLike(String Username);

    //更新用户方法  一切增删改都需要加事务处理  @Modifying就相当于@update
    @Transactional
    @Modifying
    @Query("update User u set u.username=?1 where u.id=?2")
    int Update_User(String username,int id);

    //删除用户
    @Modifying
    @Transactional
    @Query("delete from User u where u.id=?1")
    int DeleteUser(int id);


}

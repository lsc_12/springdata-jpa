package com.lsc.repository.ToandMany;

import com.lsc.pojo.ToandMany.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: spingdata_jpa
 * @description: 学生Repository
 * @author: 上河图
 * @create: 2020-10-05 17:27
 */

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {

}

package com.lsc.repository.ToandMany;

import com.lsc.pojo.ToandMany.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: spingdata_jpa
 * @description: 教师Repository
 * @author: 上河图
 * @create: 2020-10-05 17:35
 */

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Integer> {

}

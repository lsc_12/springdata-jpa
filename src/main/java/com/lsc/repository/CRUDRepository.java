package com.lsc.repository;

import com.lsc.pojo.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @program: spingdata_jpa
 * @description: CRUDRepository测试类
 * @author: 上河图
 * @create: 2020-10-04 16:21
 */
@Repository
public interface CRUDRepository extends CrudRepository<User,Integer> {

}
